const config = {
  timeout: 4500,
  ordered: false,
  defaultLoadingText: "Your server is currently loading",
  images: [
    {
      src: 'images/load.png',
      header: `<span class="uppercase">Gta</span> Online`,
      subheader : 'Subtitle of the first image',
      mainContent : 'This is new main content',
      footer : 'This is new footer',
      rightImgsrc : 'img/right-img/right-img.png',
      loadingText: "Server is currently loading"
    },
    {
      src: 'images/load2.png',
      header : `<span class="uppercase bold">Bold</span> Title`,
      subheader : 'Subtitle of the second image',
      mainContent : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer consectetur hendrerit sapien quis mattis. Nam sodales finibus eros at consectetur. Suspendisse fringilla nisi arcu, a gravida nunc tempor nec. Fusce aliquet dolor sed nisl volutpat, vitae ultrices ligula varius. Maecenas nibh quam, feugiat eu enim vel, vestibulum eleifend diam. Curabitur ut convallis arcu, et dictum dui. Nunc eget imperdiet nunc. Cras accumsan.',
      footer : 'Phasellus blandit lacus sit amet magna elementum aliquet. Suspendisse aliquam est ut ex tempus facilisis. In hac habitasse platea dictumst. Maecenas ultricies justo sed urna congue.',
      rightImgsrc : 'img/right-img/rockstar.png' ,
      loadingText: "Server is currently loading"
    }, 
    {
      src: 'images/load17.png',
      header: `<span class="uppercase">Uppercase</span> Title`,
      subheader: 'Subtitle of the second image',
      mainContent: 'Suspendisse vitae velit quam. Donec ut lectus in odio euismod accumsan. Fusce accumsan, massa sit amet bibendum porta, ipsum est dapibus metus, a sodales purus risus ac lacus. Etiam placerat tellus in ante molestie, quis facilisis nulla maximus. Vestibulum eu urna eget est suscipit tempor. Aenean blandit semper libero at posuere. Nam vitae volutpat mi. Fusce et ullamcorper lacus, non consequat justo. Nulla lobortis ligula at ligula fringilla, quis convallis nisi efficitur. Praesent lobortis posuere enim, ac malesuada augue volutpat sed. Fusce egestas volutpat auctor. Suspendisse cursus pellentesque molestie. Maecenas sodales fermentum tellus eget porttitor.',
      footer: 'This is new footer',
      rightImgsrc: 'img/right-img/rockstar.png' ,
    },
    
    {
      src: 'images/chop.png',
      header: `<span class="uppercase">Another</span> Title`,
      subheader: 'Subtitle of the random image',
      mainContent: 'Suspendisse vitae velit quam. Donec ut lectus in odio euismod accumsan. Fusce accumsan, massa sit amet bibendum porta, ipsum est dapibus metus, a sodales purus risus ac lacus. Etiam placerat tellus in ante molestie, quis facilisis nulla maximus. Vestibulum eu urna eget est suscipit tempor. Aenean blandit semper libero at posuere. ',
      footer: 'Nam vitae volutpat mi. Fusce et ullamcorper lacus, non consequat justo. Nulla lobortis ligula at ligula fringilla, quis convallis nisi efficitur. Praesent lobortis posuere enim, ac malesuada augue volutpat sed. Fusce egestas volutpat auctor. Suspendisse cursus pellentesque molestie. Maecenas sodales fermentum tellus eget porttitor.',
      rightImgsrc: 'img/right-img/right-img.png' ,
    },
    
    {
      src: 'images/franklin.png',
      header: `<span class="uppercase">Example</span> title`,
      subheader: 'Description of the title',
      mainContent: 'Praesent sit amet lectus quis elit euismod condimentum at vitae nibh. Donec eleifend lectus sed leo fringilla, ac vulputate turpis lobortis. Etiam facilisis sit amet augue efficitur iaculis. Aenean ipsum lacus, rutrum venenatis imperdiet gravida, lacinia sed arcu. Sed est orci, tincidunt in nulla nec, aliquet laoreet mauris. ',
      footer: 'Quisque sed euismod lectus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam semper egestas nulla in pellentesque.',
      rightImgsrc: 'img/right-img/rockstar.png' ,
    },
    
    {
      src: 'images/franklin2.png',
      header: `<span class="uppercase">New</span> content soon`,
      subheader: 'New content in game available soon',
      mainContent: 'Stay tuned for new updates for server',
      footer: 'We hope you will enjoy new updated stuff for the game',
      rightImgsrc: 'img/right-img/police.png' ,
    },
    
    {
      src: 'images/franklin_a.png',
      header: `<span class="uppercase">New </span> content available`,
      subheader: 'New content in game available today',
      mainContent: 'Stay tuned for new updates for server',
      footer: 'We hope you will enjoy new updated stuff for the game',
      rightImgsrc: 'img/right-img/boat.png' ,
    },

    {
      src: 'images/gtao.png',
      header: `<span class="uppercase">New </span> cars available`,
      subheader: 'Today new cars coming to the game',
      mainContent: 'Stay tuned for new server info',
      footer: 'We hope you will enjoy new updated stuff for the game',
      rightImgsrc: 'img/right-img/cars.png' ,
    },
    {
      src: 'images/images5.png',
      header: `<span class="uppercase">New </span> cars available`,
      subheader: 'Today new cars coming to the game',
      mainContent: 'Stay tuned for new server info',
      footer: 'We hope you will enjoy new updated stuff for the game',
      rightImgsrc: 'img/right-img/cars.png' ,
    },
    {
      src: 'images/images8.png',
      header: `<span class="uppercase">New </span> activity available`,
      subheader: 'Today new activity coming to the game',
      mainContent: 'Stay tuned for new server info',
      footer: 'We hope you will enjoy new updated stuff for the game',
      rightImgsrc: 'img/right-img/heist.png' ,
    },
    {
      src: 'images/images11.png',
      header: `<span class="uppercase">New </span> stuff available`,
      subheader: `Today new stuff coming fast to the game.`,
      mainContent: `Stay tuned for new info of this coming update. It is going to be lot of thigs added so we hope you are waiting and you will enjoy`,
      footer: '',
      rightImgsrc: 'img/right-img/outfit.png' ,
     
    },
    {
      src: 'images/load3.png',
      header: `<span class="uppercase">New </span> stuff available`,
      subheader: `Today new stuff coming fast to the game.`,
      mainContent: `Stay tuned for new info of this coming update. It is going to be lot of thigs added so we hope you are waiting and you will enjoy`,
      footer: 'Wait till next update day for new info',
      rightImgsrc: 'img/right-img/dynasty.png' ,
    },
    {
      src: 'images/load5.png',
      header: `<span class="uppercase">New </span> update soon`,
      subheader: `Soon new update coming fast to the game.`,
      mainContent: `Stay tuned for new info of this coming update. We hope you can wait till next update`,
      footer: 'Wait till next update day for new info',
      rightImgsrc: 'img/right-img/outfit.png' ,
    },
    {
      src: 'images/load6.png',
      header: `<span class="uppercase">New </span> update soon`,
      subheader: `Soon new update coming fast to the game.`,
      mainContent: `Stay tuned for new info of this coming update. We hope you can wait till next update`,
      footer: 'Wait till next update day for new info',
      rightImgsrc: 'img/right-img/outfit.png' ,
    },
    {
      src: 'images/load7.png',
      header: `<span class="uppercase">New </span> release soon`,
      subheader: `Soon game will invite new release.`,
      mainContent: `Fivem is updating so we will give you some reds for logging to the game this week`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/shark.png' ,
    },
    {
      src: 'images/load8.png',
      header: `<span class="uppercase">Free </span> cash soon`,
      subheader: `Soon you will get free cash from us.`,
      mainContent: `This and the next week we will give you free money`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/cash.png' ,
    },
    {
      src: 'images/load13.png',
      header: `<span class="uppercase">Free </span> car soon`,
      subheader: `Soon you will get free car from us.`,
      mainContent: `This and the next week we will give you free car`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/cars.png' ,
    },
    {
      src: 'images/load18.jpg',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/director_mode.png',
    },
    {
      src: 'images/loading.png',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/dynasty.png' ,
    },
    {
      src: 'images/michael.png',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/freemode.png' ,

    },
    {
      src: 'images/michael2.png',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/mazebank.png' ,
      
    },
    {
      src: 'images/michael3.png',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/police.png' ,

    },
    {
      src: 'images/nervousRon.jpg',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/right-img.png' ,

    },
    {
      src: 'images/online.jpg',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/rockstar_logo.png' ,

    },
    {
      src: 'images/trevor.jpg',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/rockstar.png' ,

    },
    {
      src: 'images/trevor3.png',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/social_club.png' ,

    },
    {
      src: 'images/wade.jpg',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/boat.png' ,

    },
    {
      src: 'images/dave.jpg',
      header: `<span class="uppercase">Free </span> gift soon`,
      subheader: `Soon you will get free gift from us.`,
      mainContent: `This and the next week we will give you free gift`,
      footer: 'We belive that you come for the reward',
      rightImgsrc: 'img/right-img/freemode.png' ,

    }
    
  ]
}
