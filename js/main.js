// Grab all the content of the website to variables
const header = document.querySelector('.header');
const subheader = document.querySelector('.subheader');
const mainContent = document.querySelector('.main');
const footer = document.querySelector('.footer');
const rightImg = document.getElementById('rightImg');
const loadImg = document.getElementById('loadImage');
const loader = document.querySelector('.loader');
const loading = document.querySelector('.loading');

window.addEventListener('DOMContentLoaded', () => {
  let images = config.images;
  let random = Math.floor(Math.random() * config.images.length);
  header.innerHTML = images[random].header;
  subheader.innerHTML = images[random].subheader;
  mainContent.innerHTML = images[random].mainContent;
  footer.innerHTML= images[random].footer;
  loadImg.src = images[random].src;
  rightImg.src = images[random].rightImgsrc;
  if (images[random].loadingText !== undefined) {
    loading.innerText = images[random].loadingText;
  } else {
    loading.innerText = config.defaultLoadingText;
  }
})


if (config.ordered) {
  let images = config.images;
  let i = 0;
  setInterval(() => {
    
    header.innerHTML = images[i].header;
    subheader.innerHTML = images[i].subheader;
    mainContent.innerHTML = images[i].mainContent;
    footer.innerHTML= images[i].footer;
    loadImg.src = images[i].src;
    rightImg.src = images[i].rightImgsrc;
    i++;

    if (i === images.length) {
      i = 0;
    }
  }, config.timeout);
}else {

  setInterval(() => {
    
    let images = config.images;
    let random = Math.floor(Math.random() * config.images.length)
    header.innerHTML = images[random].header;
    subheader.innerHTML = images[random].subheader;
    mainContent.innerHTML = images[random].mainContent;
    footer.innerHTML= images[random].footer;
    loadImg.src = images[random].src;
    rightImg.src = images[random].rightImgsrc;
    if (images[random].loadingText !== undefined) {
      loading.innerText = images[random].loadingText;
    } else {
      loading.innerText = config.defaultLoadingText;
    }
  }, config.timeout);
}