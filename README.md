<div align="center"><h1>LOADING SCREEN</h1></div>

This is loading screen for **FiveM** inspired by **GTA Online**. It is easy to configure for your own purpose. Displays random images from object that you pass, example inside `config.js`
</br>
</br>

# Instalation
To install this loading screen you need to download it from gitlab, place it into your server resource folder and add to `server.cfg` following line (if you rename folder replace gtao-loadingscreen with folder name that you rename to): 

```bash
start gtao-loadingscreen
```

Then open your server and test it out.

If you want to change something just look at the `config.js` and `config.css`.

If you want to change text or photos that are visible on the loading screen, you should look inside `config.js` file.

If you want to add something you need to copy and adapt to your needs.

```javascript
// Example value from config.js
{
      src: 'images/load.png', // source of your background image
      header: `<span class="uppercase">Gta</span> Online`, // If you want to display your header as uppercase surround with span, text between span wil be display as uppercase (ex. GTA)
      subheader : 'Subtitle of the first image', // Here place your subheader, text that will display under big header, if not needed leave empty.
      mainContent : 'This is new main content', // Here is main content, long text that will display under subheader
      footer : 'This is new footer', // short text, something like summary, if not needed leave empty.
      rightImgsrc : 'img/right-img/right-img.png', // small image that will display on the right of text
      loadingText: "Server is currently loading" // Text that will display in the bottom right of screen
},
```
If you are modifying `config.js` file and adding new stuff, you need to put new values between **{}** and after each **}** need to put **,**

Also it needs to be put between **[]**. Read more about array of objects in javascript.

You don't need to change anything in `main.js` file. All data that is visible on the screen is inside `config.js`. This file needs to be change for customization.

If you want to change some basic styles like colors of font checkout `config.css`

# Preview
To see preview of loading screen click [here](https://czesiek2000.gitlab.io/-/gtao-loadingscreen/-/jobs/248443788/artifacts/public/index.html)

Some things doesn't work well and diferent than in fivem game and cannot work well on other devices than desktop screens.

Some picture to preview how it looks like

![preview1](./docs/1.png)
![preview1](./docs/2.png)
![preview1](./docs/3.png)

# Known isues 
* Style of the images are in browser good but in game are smaller on the bottom


# Licence 
This is repository is under **MIT** Licence. 

If you like it you can leave star and some credits on the fivem forum or here will be nice.