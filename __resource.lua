resource_manifest_version '05cfa83c-a124-4cfa-a768-c24a5811d8f9'

description 'Simple gta online loading screen based on game'

files {
  'index.html',

  'styles.css',
  'config.css',
  
  'config.js',
  'main.js',

  'images/load.png',
  'images/load2.png',
  'images/load17.png',
  'images/chop.png',
  'images/franklin.png',
  'images/franklin2.png',
  'images/franklin_a.png',
  'images/gtao.png',
  'images/images5.png',
  'images/images8.png',
  'images/images11.png',
  'images/load3.png',
  'images/load5.png',
  'images/load6.png',
  'images/load7.png',
  'images/load8.png',

  'img/right-img/right-img.png',
  'img/right-img/fleeca.png',
  'img/right-img/rockstar.png',
  'img/right-img/rockstar-logo.png',
  'img/right-img/boat.png',
  'img/right-img/cars.png',
  'img/right-img/heist.png',

}

loadscreen 'index.html'